import java.util.ArrayList;
import java.util.List;

public class User {

    private String name;

    private List<Project> projects = new ArrayList<>();

    private List<Issue> issues = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    public void setIssues(List<Issue> issues) {
        this.issues = issues;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name +
                '}';
    }
}
