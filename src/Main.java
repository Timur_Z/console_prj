import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {


    public static void main(String[] args) throws IOException {

        Project project = new Project();

        project.setName("Бухгалтерская программа");

        List<Project> projectList = new ArrayList<>();
        projectList.add(project);

        User one = new User();
        one.setName("Коля");
        User two = new User();
        two.setName("Вася");

        List<User> users = new ArrayList<>();
        users.add(one);
        users.add(two);

//        project.getUsers().add(one);
//        project.getUsers().add(two);

        project.setUsers(Arrays.asList(one,two));

        one.setProjects(projectList);
        two.setProjects(projectList);

        Issue issueOne = new Issue();
        issueOne.setName("issue_one name");
        issueOne.setDescription("issue_one description");
        issueOne.setProject(project);
        Issue issueTwo = new Issue();
        issueTwo.setName("issue_two name");
        issueTwo.setDescription("issue_two description");
        issueTwo.setProject(project);
        Issue issueThree = new Issue();
        issueThree.setName("issue_three name");
        issueThree.setDescription("issue_three description");
        issueThree.setProject(project);

        issueOne.setUser(one);
        issueTwo.setUser(two);

        List<Issue> issues = new ArrayList<>();
        issues.add(issueOne);
        issues.add(issueTwo);
        issues.add(issueThree);

        System.out.println(project);
        System.out.println(issueOne);
        System.out.println(one);


        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true){
            /* реализовать обработку ввода пользлвателя для слов Project, Issue,User и Quit
            должно выводить список всех проектов, юзеров и ишью.
            так же реализовать обработку слова report ( project->user>issue)
             */
            String word = reader.readLine();
            if (word.equalsIgnoreCase("project")){
                for (Project pr : projectList){
                    System.out.println(pr);
                }

            } else if (word.equalsIgnoreCase("issue")){

            }else if (word.equalsIgnoreCase("user")){

            }else if (word.equalsIgnoreCase("quit")){
                break;
            }else {
                System.out.println("Введите верную команду!");
                System.out.println("project/user/issue/quit");
            }
        }



    }
}
